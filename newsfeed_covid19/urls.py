from django.urls import path
from django.contrib import admin
from main import views


urlpatterns = [
    path('', views.index),
    path('admin/', admin.site.urls),
    path('populate/', views.populate_database, name="populate"),
    path('update/', views.update_database),
    path('feed/', views.feed, name="feed"),
    path('search/', views.search),
    path('likes/', views.likes),
    path('like/', views.like, name="like"),
    path('charts/', views.charts)
]

