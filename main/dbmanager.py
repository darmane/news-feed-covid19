from main.scrapper import extract_all
from main.models import News, Quantities, Likes
from main.indexer import load_indexer, clear_indexer


def delete_news():
    try:
        print('Deleting news table...')
        News.objects.all().delete()
        print('Done.')
    except Exception as e:
        print(e)


def delete_quantities():
    try:
        print('Deleting quantities table...')
        Quantities.objects.all().delete()
        print('Done.')
    except Exception as e:
        print(e)


def delete_likes():
    try:
        print('Deleting likes table...')
        Likes.objects.all().delete()
        print('Done.')
    except Exception as e:
        print(e)


def delete_database():
    print('\nDELETING DB ----------------------------------------------')
    delete_news()
    delete_quantities()
    delete_likes()
    clear_indexer()
    print('\nDB deleted ----------------------------------------------')


def insert_into_database(n, q):
    numn = 0
    try:
        print('\nAdding data to DB...')
        News.objects.bulk_create(n)
        Quantities.objects.bulk_create(q)
        numn = len(n)
        print('Entries', News.objects.count(), Quantities.objects.count())
        print('Done.')
    except Exception as e:
        print(e)
    return numn


def populate_database():
    print('\nPOPULATING DB ----------------------------------------------')
    delete_database()
    news, quant = extract_all()
    numNews = insert_into_database(news, quant)
    load_indexer(news)
    print('\nDB populated ----------------------------------------------')
    return str(numNews)


def update_database():
    print('\nUPDATING DB ----------------------------------------------')
    news, quant = extract_all()
    lastnews = News.objects.all().order_by('-date')[0]
    news = [n for n in news if n.date > lastnews.date]
    numnews = insert_into_database(news, quant)
    load_indexer(news)
    print('\nDB updated ----------------------------------------------')
    return str(numnews)



