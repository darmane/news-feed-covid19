from whoosh.fields import Schema, TEXT, DATETIME, NUMERIC
import os
from whoosh import index
import csv
from datetime import datetime


path_data = 'csv/cvdata.csv'
index_news_dir = 'indexnewsdir'
index_data_dir = 'indexdatadir'


def load_news(news):
    print('Indexing news documents...')

    # create schema
    schema = Schema(
        title=TEXT(stored=True),
        content=TEXT(stored=True),
        date=DATETIME(stored=True))

    # create index
    if not os.path.exists(index_news_dir):
        os.mkdir(index_news_dir)
        ix = index.create_in(index_news_dir, schema)
    else:
        ix = index.open_dir(index_news_dir)

    # create writer
    writer = ix.writer()

    # indexing
    for n in news:
        writer.add_document(title=n.title, content=n.content, date=n.date)

    # commit writer
    writer.commit()

    print('Done.')


def load_data():
    print('Indexing data documents...')

    # create schema
    schema = Schema(
        community=TEXT(stored=True),
        date=DATETIME(stored=True),
        pcr=NUMERIC(stored=True),
        testac=NUMERIC(stored=True),
        hospital=NUMERIC(stored=True),
        uci=NUMERIC(stored=True),
        death=NUMERIC(stored=True))

    # create index
    if not os.path.exists(index_data_dir):
        os.mkdir(index_data_dir)
        ix = index.create_in(index_data_dir, schema)
    else:
        ix = index.open_dir(index_data_dir)

    # create writer
    writer = ix.writer()

    # indexing
    try:
        fileobj = open(path_data, 'r')
        reader = csv.reader(fileobj, delimiter=',')
        for l in list(reader)[1:]:
            try:
                data = []
                for x in l[2:]:
                    if x != '':
                        data.append(int(x))
                    else:
                        data.append(0)
                community, date, pcr, testac, hospital, uci, death = l[:2] + data
                datetm = datetime.strptime(date, "%d/%m/%Y")
                writer.add_document(community=community, date=datetm, pcr=pcr, testac=testac, hospital=hospital, uci=uci, death=death)
            except Exception as e:
                print(e)
        fileobj.close()
    except Exception as e:
        print(e)

    # commit writer
    writer.commit()

    print('Done.')


def clear_news():
    print('Clearing news indexer...')

    # create schema
    schema = Schema(
        title=TEXT(stored=True),
        content=TEXT(stored=True),
        date=DATETIME(stored=True))

    # create index
    if not os.path.exists(index_news_dir):
        os.mkdir(index_news_dir)
    index.create_in(index_news_dir, schema)

    print('Done.')


def clear_data():
    print('Clearing data indexer...')

    # create schema
    schema = Schema(
        community=TEXT(stored=True),
        date=DATETIME(stored=True),
        pcr=NUMERIC(stored=True),
        testac=NUMERIC(stored=True),
        hospital=NUMERIC(stored=True),
        uci=NUMERIC(stored=True),
        death=NUMERIC(stored=True))

    # create index
    if not os.path.exists(index_data_dir):
        os.mkdir(index_data_dir)
    index.create_in(index_data_dir, schema)

    print('Done.')


def load_indexer(news):
    load_news(news)
    load_data()


def clear_indexer():
    clear_news()
    clear_data()

