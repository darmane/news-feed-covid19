from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator
import main.dbmanager as dbmanager
from main.forms import SearchForm, ChartsForm
from main.models import News, Quantities, Likes
from whoosh.qparser import MultifieldParser, QueryParser
from whoosh import index as whindex
from whoosh import sorting


index_news_dir = 'indexnewsdir'
index_data_dir = 'indexdatadir'


def index(request):
    return render(request, 'index.html', {'tab': 0})


def populate_database(request):
    if 'accept' in request.POST:
        numnews = dbmanager.populate_database()
        return render(request, 'confirmation.html', {'numnews': numnews, 'tab': 1})
    return render(request, 'populate.html', {'tab': 1})


def update_database(request):
    numnews = dbmanager.update_database()
    return redirect('/feed/?page=' + str(1) + '&update=' + str(numnews))


def feed(request):

    try:
        # Update notification
        numnupdate = request.GET.get('update')

        # charts
        qtl = Quantities.objects.filter(area='local').order_by('-date').first()
        qtg = Quantities.objects.filter(area='global').order_by('-date').first()
        data = [[qtl.confirmed, qtg.confirmed], [qtl.deaths, qtg.deaths], [qtl.healed, qtg.healed]]
        labels = ['España', 'Mundo']

        # news
        news = News.objects.all().order_by('-date')
        paginator = Paginator(news, 25)  # Show 25 per page.
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        lks = []
        for n in page_obj:
            lk = 1 if Likes.objects.filter(news=n).count() > 0 else 0
            lks.append(lk)
        news = zip(page_obj, lks)
        return render(request, 'feed.html',
                      {'labels': labels, 'data': data, 'page_obj': page_obj, 'news': news, 'page_number': page_number, 'tab': 3, 'nupdate': numnupdate})
    except Exception as e:
        print(e)
        return render(request, 'feed.html',
                      {'labels': [], 'data': [], 'page_obj': None, 'news': [], 'page_number': 0})


def search(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            ix = whindex.open_dir(index_news_dir)
            try:
                qp = MultifieldParser(["title", "content"], schema=ix.schema)
                q = qp.parse(form.cleaned_data['content'])
                with ix.searcher() as s:
                    facet = sorting.FieldFacet("date", reverse=True)
                    results = [[r['date'], r['title'], r['content']] for r in s.search(q, sortedby=facet)]
                return render(request, 'search.html', {'form': form, 'results': results, 'search': 1, 'tab': 4})
            except Exception as e:
                print(e)
            finally:
                ix.close()
    else:
        form = SearchForm()
    return render(request, 'search.html', {'form': form, 'search': 0, 'tab': 4})


def likes(request, page=None):
    lks = Likes.objects.select_related('news').all()
    news = [lk.news for lk in lks]
    paginator = Paginator(news, 10)  # Show 10 per page.
    page_number = page if page is not None else request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'likes.html', {'page_obj': page_obj, 'page_number': page_number, 'tab': 5})


def like(request):
    news_id = request.POST.get('news_id', -1)
    page_number = request.POST.get('page', -1)
    router = request.POST.get('router', -1)
    if news_id != -1 and page_number != -1:
        news = get_object_or_404(News, pk=news_id)
        lks = Likes.objects.filter(news=news)
        page_number = 1 if page_number == "None" else page_number
        if lks.count() <= 0:
            lk = Likes(news=news)
            lk.save()
        else:
            lks.delete()
        if router == '0':
            return redirect('/feed/?page='+str(page_number))
        else:
            return redirect('/likes/?page='+str(page_number))
    return render(request, 'index.html')


def charts(request):
    if request.method == 'POST':
        form = ChartsForm(request.POST)
        if form.is_valid():
            community = form.cleaned_data['community']
            ix = whindex.open_dir(index_data_dir)
            try:
                qp = QueryParser("community", schema=ix.schema)
                with ix.searcher() as s:
                    data = [[], [], []]
                    labels = []
                    if not community == "ES":
                        q = qp.parse(community)
                        for r in s.search(q, sortedby="date", limit=500):
                            data[0].append(r['pcr'])
                            data[1].append(r['hospital'])
                            data[2].append(r['death'])
                            labels.append(r['date'].strftime('%d-%b-%Y'))
                    else:
                        community = '[AD TO RI]'
                        q = qp.parse(community)
                        results = s.search(q, groupedby="date", limit=500).groups('date')
                        for r in results:
                            pcr = 0
                            hpt = 0
                            dth = 0
                            for i in results[r]:
                                doc = s.stored_fields(i)
                                pcr += doc['pcr']
                                hpt += doc['hospital']
                                dth += doc['death']
                            data[0].append(pcr)
                            data[1].append(hpt)
                            data[2].append(dth)
                            labels.append(r.strftime('%d-%b-%Y'))

                    inc = [[data[0][0]], [data[1][0]], [data[2][0]]]
                    for i in range(1, len(data[0])):
                        j = i - 1
                        inc[0].append(abs(data[0][i] - data[0][j]))
                        inc[1].append(abs(data[1][i] - data[1][j]))
                        inc[2].append(abs(data[2][i] - data[2][j]))
                    return render(request, 'charts.html', {'form': form, 'data': data, 'data_inc': inc, 'labels': labels, 'tab': 6})
            except Exception as e:
                print(e)
            finally:
                ix.close()
    else:
        form = ChartsForm()
    return render(request, 'charts.html', {'form': form, 'tab': 6})