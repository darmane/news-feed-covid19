from django.db import models


class News(models.Model):
    title = models.CharField(max_length=200, null=False)
    content = models.CharField(max_length=1024, null=False)
    date = models.DateTimeField(null=False)

    def __str__(self):
        return self.title + self.date.__str__()


class Likes(models.Model):
    news = models.ForeignKey('News', on_delete=models.CASCADE)

    def __str__(self):
        return self.news.__str__()


class Quantities(models.Model):
    area = models.CharField(max_length=10, choices=[('LOCAL', 'local'), ('GLOBAL', 'global')])
    confirmed = models.IntegerField(null=False)
    deaths = models.IntegerField(null=False)
    healed = models.IntegerField(null=False)
    date = models.DateTimeField('date published', null=False)

    def __str__(self):
        return 'Area: ' + str(self.area) \
               + ' -> confirmed: ' + str(self.confirmed)\
               + ' - deaths: ' + str(self.deaths) \
               + ' - healed: ' + str(self.healed) \
               + '\n' + 'date: ' + self.date.__str__()
