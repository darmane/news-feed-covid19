from bs4 import BeautifulSoup
import urllib
from datetime import datetime, timedelta, timezone
from main.models import News, Quantities
import os
from requests import get
import csv


url_news = 'https://www.abc.es/'
url_data = 'https://cnecovid.isciii.es/covid19/#documentaci%C3%B3n-y-datos'

def extract_news():
    print('\nExtracting news from the web...')

    f = urllib.request.urlopen(url_news)
    soup1 = BeautifulSoup(f, 'html.parser')
    newsurl = soup1.find('aside', class_='voc-multimedia-detail voc-basic-module voc-carousel-crisis-dwc').find('a')['href']
    newssite = urllib.request.urlopen(newsurl)
    soup2 = BeautifulSoup(newssite, 'html.parser')

    # News
    news = []
    date = None
    i = 1
    for n in soup2.find_all('p', class_='entrada-directo'):
        try:
            title = n.find('span', class_='ladillo')
            title = "" if title is None else title.text.strip()
            content = ''.join([str(s) for s in n.contents[2:]]).replace('<strong>', "").replace('</strong>', "")
            time = n.find('span', class_='minuto').text.strip().split('.')
            hour, minute = int(time[0]), int(time[1])
            if date is None:
                date = datetime.today().replace(hour=hour, minute=minute, second=0, microsecond=0, tzinfo=timezone.utc)
            else:
                if date.hour < hour:
                    date = date.replace(hour=hour, minute=minute, tzinfo=timezone.utc)
                    date = date - timedelta(days=1)
                else:
                    date = date.replace(hour=hour, minute=minute, tzinfo=timezone.utc)
            news.append(News(title=title, content=content, date=date))
            print('extracted', i)
            i += 1
        except Exception as e:
            print(str(e))

    # Numbers
    quantities = []
    for q in soup1.find_all('div', class_='voc-updatedata-coronavirus__container-info-data'):
        quantities.append(int(q.find('p').text.strip().replace('.', '')))
    try:
        loc = Quantities(area='local', confirmed=quantities[0], deaths=quantities[1], healed=quantities[2], date=datetime.now())
        glob = Quantities(area='global', confirmed=quantities[3], deaths=quantities[4], healed=quantities[5], date=datetime.now())
    except Exception as e:
        print(e)
    print('Done.')
    return news, [loc, glob]


def extract_data():
    print("\Extracting data from the web...")

    file = urllib.request.urlopen(url_data)
    url = BeautifulSoup(file, 'html.parser').find('div', id='datos').find('a')['href']

    if not os.path.exists("csv"):
        os.mkdir("csv")

    response = get(url)
    with open('csv/cvdata.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        for line in response.iter_lines():
            try:
                row = line.decode('utf-8').split(',')
                if row[0] == 'AN': row[0] = "AD"
                writer.writerow(row[:2]+row[3:])
            except Exception as e:
                print(e)

    print('Done.')

def extract_all():
    extract_data()
    return extract_news()
