from django import forms


class SearchForm(forms.Form):
    content = forms.CharField(label="", widget=forms.TextInput(
        attrs={'placeholder': 'Ej: estado...', 'class': 'form-control mr-sm-2'}), max_length=100)


class ChartsForm(forms.Form):
    community = forms.CharField(
        label='',
        widget=forms.Select(
            choices=[
                ("ES", "Nacional"),
                ("AD", "Andalucía"),
                ("AR", "Aragón"),
                ("AS", "Asturias"),
                ("IB", "Islas Baleares"),
                ("CN", "Islas Canarias"),
                ("CB", "Cantabria"),
                ("CM", "Castilla-La Mancha"),
                ("CL", "Castilla y León"),
                ("CT", "Cataluña"),
                ("CE", "Ceuta"),
                ("VC", "Comunidad Valenciana"),
                ("EX", "Extremadura"),
                ("GA", "Galicia"),
                ("MD", "Comunidad de Madrid"),
                ("ML", "Melilla"),
                ("MC", "Región de Murcia"),
                ("NC", "Comunidad Foral de Navarra"),
                ("PV", "País Vasco"),
                ("RI", "La Rioja")],
            attrs={'class': "btn btn-primary dropdown-toggle"}
        )
    )