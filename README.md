# News Feed Covid19

Web scrapper and frontend for visualization of news and data on covid 19.

## Stack

- DJango
- Python

## License

MIT License.

You can fork this project for free under the following conditions:

- Add a link to this project.
- The software is provided on an "as is" basis and without warranty of any kind. Use it at your own risk.
